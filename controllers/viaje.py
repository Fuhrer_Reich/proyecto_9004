# -*- coding: utf-8 -*-

def index():
	rows = db().select(db.viaje.ALL, orderby=db.viaje.id)
	return dict(misregistros = rows)

def crear():
	if request.post_vars:
		# Insertar
		db.viaje.insert(
			persona_id = request.post_vars.persona_va,
			fecha = request.post_vars.fecha
		)

	personas = db().select(db.persona.ALL, orderby=db.persona.nombre)
	return dict(personas = personas)

def editar():
	viaje_id 	= request.args(0)
	if request.post_vars:
		# Actualizar
		db(db.viaje.id == viaje_id).update(
			persona_id = request.post_vars.persona_va,
			fecha = request.post_vars.fecha
		)

	viaje_sel = db.viaje(viaje_id)
	personas = db().select(db.persona.ALL, orderby=db.persona.nombre)

	return dict(miregistro = viaje_sel, personas = personas)

def eliminar():
	viaje_id 	= request.post_vars.viaje_id
	db(db.viaje.id == viaje_id).delete()
	return True