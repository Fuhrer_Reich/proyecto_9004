# -*- coding: utf-8 -*-

def index():
	rows = db().select(db.pedido.ALL, orderby=db.pedido.id)
	return dict(misregistros = rows)

def crear():
	if request.post_vars:
		# Insertar
		db.pedido.insert(
			viaje_id 	= request.post_vars.viaje,
			persona_id 	= request.post_vars.solicitante,
			monto 		= request.post_vars.monto,
			comision	= request.post_vars.comision
		)

	personas 	= db().select(db.persona.ALL, orderby=db.persona.nombre)
	viajes		= db().select(db.viaje.ALL, orderby=db.viaje.fecha)
	return dict(personas = personas, viajes = viajes)

def editar():
	pedido_id 	= request.args(0)
	if request.post_vars:
		# Actualizar
		db(db.pedido.id == pedido_id).update(
			viaje_id 	= request.post_vars.viaje,
			persona_id 	= request.post_vars.solicitante,
			monto 		= request.post_vars.monto,
			comision	= request.post_vars.comision
		)

	pedido_sel = db.pedido(pedido_id)
	personas 	= db().select(db.persona.ALL, orderby=db.persona.nombre)
	viajes		= db().select(db.viaje.ALL, orderby=db.viaje.fecha)

	return dict(miregistro = pedido_sel, personas = personas, viajes = viajes)

def eliminar():
	pedido_id 	= request.post_vars.pedido_id
	db(db.pedido.id == pedido_id).delete()
	return True


def detalle_pedido_index():
	pedido_id 	= request.args(0)
	pedido_sel = db.pedido(pedido_id)
	detalle	= db(db.detalle_pedido.pedido_id == pedido_id).select(db.detalle_pedido.ALL, orderby=db.detalle_pedido.id)
	return dict(pedido_sel = pedido_sel, detalle = detalle)

def add_producto():
	pedido_id 	= request.args(0)
	if request.post_vars:
		# Insertar
		db.detalle_pedido.insert(
			pedido_id 	= pedido_id,
			producto_id	= request.post_vars.producto,
			cantidad 	= request.post_vars.cantidad,
			costo 		= 0,
			comprado 	= 'F'
		)

	productos = db().select(db.producto.ALL, orderby=db.producto.descripcion)	
	return dict(productos = productos)

def del_producto():
	detalle_pedido_id 	= request.post_vars.detalle_pedido_id
	db(db.detalle_pedido.id == detalle_pedido_id).delete()
	return True

def aplica_producto():
	detalle_pedido_id 	= request.args(0)
	if request.post_vars:
		# Actualizar
		if float(request.post_vars.costo) > 0:
			compra = 'T'
		else:
			compra = 'F'

		db(db.detalle_pedido.id == detalle_pedido_id).update(
			costo 		= request.post_vars.costo,
			comprado 	= compra
		)
		redirect(URL('pedido','detalle_pedido_index',args=request.post_vars.pedido_id))

	detalle_pedido_sel = db.detalle_pedido(detalle_pedido_id)
	return dict(miregistro = detalle_pedido_sel)


# query directo ejemplo:
# rows = db.executesql("select * from tabla") --> esto para consulta normal
# rows = db.executesql("select * from tabla", as_dict = True) 
# 	--> cuando es solo un registro y se desea usar como diccionario