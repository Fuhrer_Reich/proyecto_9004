# -*- coding: utf-8 -*-

def index():
	rows = db().select(db.producto.ALL, orderby=db.producto.descripcion)
	return dict(misregistros = rows)

def crear():
	if request.post_vars:
		# Insertar
		db.producto.insert(
			descripcion = request.post_vars.descripcion,
			precio = request.post_vars.precio
		)
	return dict()

def editar():
	producto_id 	= request.args(0)
	if request.post_vars:
		# Actualizar
		db(db.producto.id == producto_id).update(
			descripcion = request.post_vars.descripcion,
			precio = request.post_vars.precio
		)

	producto_sel = db.producto(producto_id)
	return dict(miregistro = producto_sel)

def eliminar():
	producto_id 	= request.post_vars.producto_id
	db(db.producto.id == producto_id).delete()
	return True