# -*- coding: utf-8 -*-

def index():
	rows = db().select(db.persona.ALL, orderby=db.persona.nombre)
	return dict(misregistros = rows)

def crear():
	if request.post_vars:
		# Insertar
		db.persona.insert(
			nombre = request.post_vars.nombre
		)
	return dict()

def editar():
	persona_id 	= request.args(0)
	if request.post_vars:
		# Actualizar
		db(db.persona.id == persona_id).update(
			nombre = request.post_vars.nombre
		)

	persona_sel = db.persona(persona_id)
	return dict(miregistro = persona_sel)

def eliminar():
	persona_id 	= request.post_vars.persona_id
	db(db.persona.id == persona_id).delete()
	return True